import {Routes} from "@angular/router";

export const ROUTES: Routes = [
  {path: 'login', loadChildren: () => import('./auth/login/login.module').then(l => l.LoginModule)},
  {path: '**', redirectTo: '/'}
];
